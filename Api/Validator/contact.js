const joi = require('@hapi/joi');

const contactValidator = (data) => {
    const schema = joi.object({
        name: joi.string().min(6).required(),
        phoneNumber: joi.string().min(12).max(15).required(),
        address: joi.string().min(12).max(255).required(),
    });
    return schema.validate(data);
};

const contactupdateValidator = (data) => {
    const schema = joi.object({
        id: joi.required(),
        name: joi.string().min(6).required(),
        phoneNumber: joi.string().min(12).max(15).required(),
        address: joi.string().min(12).max(255).required(),
    });
    return schema.validate(data);
};


module.exports.contactValidator = contactValidator;
module.exports.contactupdateValidator = contactupdateValidator;