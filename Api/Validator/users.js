const joi = require('@hapi/joi');

const RegisterValidator = (data) => {
    const schema = joi.object({
        name: joi.string().min(6).required(),
        email: joi.string().min(6).required().email(),
        password: joi.string().min(6).required()
    });
    return schema.validate(data);
};

const LoginValidator = (data) => {
    const schema = joi.object({
        email: joi.string().min(6).required().email(),
        password: joi.string().min(6).required()
    });
    // console.log('masuk pak')
    return schema.validate(data);
};

module.exports.RegisterValidator = RegisterValidator;
module.exports.LoginValidator = LoginValidator;