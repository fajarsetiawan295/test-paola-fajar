const mongoose = require('mongoose');
const mongoosePaginate = require("mongoose-paginate-v2");

const contactSchemaa = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 6,
    },
    picture: {
        type: String,
        mix: 255,
        min: 6
    },
    phoneNumber: {
        type: String,
        required: true,
        max: 15,
        min: 12
    },
    address: {
        type: String,
        required: true,
        max: 15,
        min: 12
    },
    date: {
        type: Date,
        default: Date.now
    }

});
contactSchemaa.methods.toJSON = function () {
    var obj = this.toObject();
    return obj;
}
contactSchemaa.plugin(mongoosePaginate);
module.exports = mongoose.model('Contact', contactSchemaa);