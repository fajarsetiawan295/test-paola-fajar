const mongoose = require('mongoose');

const UserSchemaa = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 6,
    },
    email: {
        type: String,
        required: true,
        mix: 255,
        min: 6
    },
    password: {
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    profiles: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
    restorans: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Restoran' }],
    date: {
        type: Date,
        default: Date.now
    }

});
UserSchemaa.methods.toJSON = function () {
    var obj = this.toObject();
    delete obj.password;
    delete obj.email
    delete obj.profiles._id
    delete obj.profiles.users
    return obj;
}
module.exports = mongoose.model('User', UserSchemaa);