const router = require("express").Router();
const { contactValidator, contactupdateValidator } = require("../Validator/contact");
const token = require("../../Config/Security/VerifyToken");
const contact = require("../Model/contact");
const fs = require('fs')

const multer = require("multer");
const express = require("express");
const app = express();

const getPagination = (page, size) => {
    const limit = size ? +size : 10;
    const offset = page ? page * limit : 0;

    return { limit, offset };
};

/**
 * @swagger
 * components:
 *   schemas:
 *     contact:
 *       type: object
 *       required:
 *         - name
 *         - phoneNumber
 *         - address
 *       properties:
 *         name:
 *           type: string
 *           description: The User name
 *         address:
 *           type: string
 *           description: The User address
 *         phoneNumber:
 *           type: string
 *           description: The User phoneNumber
 *       example:
 *         name: fajarsetiawan
 *         address: "jalan haji saya"
 *         phoneNumber: "082213100769"
 */

/**
 * @swagger
 * tags:
 *   name: Contact
 *   description: The Contact managing API
 */

/**
 * @swagger
 * /api/contact/create:
 *   post:
 *     summary: Create a new Contact
 *     tags: [Contact]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/contact'
 *     security:
 *      -  basicAuth: []
 *     responses:
 *       200:
 *         description: The Contact was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/contact'
 *       500:
 *         description: Some server error
 */
router.post("/create", token, async (req, res) => {
    //lets validate data
    const { error } = contactValidator(req.body);
    if (error)
        return res.status(400).send({
            status: 400,
            error: error.details[0].message,
        });

    // array create
    const create = new contact({
        name: req.body.name,
        phoneNumber: req.body.phoneNumber,
        address: req.body.address,
        picture: null,
    });

    try {
        // save to databse
        const saveData = await create.save();

        // retrun true save database
        res.status(200).send({
            status: 200,
            data: saveData,
        });
    } catch (err) {
        // retrun false save database
        res.status(400).send(err);
    }
}); //end route Create

/**
 * @swagger
 * /api/contact/list:
 *   get:
 *     summary: Get the Contact by name and phone number 
 *     tags: [Contact]
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         required: false
 *         description: The contact name
 *       - in: query
 *         name: phone
 *         schema:
 *           type: string
 *         required: false
 *         description: The contact phone
 *       - in: query
 *         name: page
 *         schema:
 *           type: int
 *         required: false
 *         description: The contact page
 *     security:
 *      -  basicAuth: []
 *     responses:
 *       200:
 *         description: The contact description by name
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/contact'
 *       404:
 *         description: The contact was not found
 */
router.get("/list", token, async (req, res) => {
    //check contact
    const { page, size, name, phone } = req.query;
    var condition = { name: { $regex: new RegExp(name), $options: "i" }, phoneNumber: { $regex: new RegExp(phone), $options: "i" } }

    const { limit, offset } = getPagination(page, size);
    await contact.paginate(condition, { offset, limit })
        .then(result => {
            res.status(200).send({
                status: 200,
                data: result,
            })
        })
        .catch(error => {
            return res.status(400).send({
                satatus: 400,
                error: "error :" + error.message,
            });
        });

});

/**
 * @swagger
 * /api/contact/detail:
 *   get:
 *     summary: Get the Contact view detail
 *     tags: [Contact]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *         required: false
 *         description: The contact id
 *     security:
 *      -  basicAuth: []
 *     responses:
 *       200:
 *         description: The contact description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/contact'
 *       404:
 *         description: The contact was not found
 */
router.get("/detail", token, async (req, res) => {

    //check contact
    await contact.findById(req.query.id).exec(function (err, data) {
        // data kosong
        if (err || !contact)
            return res.status(400).send({
                satatus: 400,
                error: "empaty data",
            });
        res.status(200).send({
            status: 200,
            data: data,
        });
    });
});

/**
 * @swagger
 * /api/contact/delete:
 *   get:
 *     summary: Get the Contact delete
 *     tags: [Contact]
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *         required: false
 *         description: The contact id
 *     security:
 *      -  basicAuth: []
 *     responses:
 *       200:
 *         description: The contact description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/contact'
 *       404:
 *         description: The contact was not found
 */
router.get("/delete", token, async (req, res) => {
    //check contact

    await contact.deleteOne({ _id: req.query.id }).exec(function (err, data) {
        // data kosong
        if (err || !contact)
            return res.status(400).send({
                satatus: 400,
                error: "empaty data",
            });



        // delete img
        contact.findById(req.query.id).exec(function (err, data) {
            fs.unlink(data.picture, (err) => {
                if (err) {
                    console.error(err)
                }
            });
        });

        res.status(200).send({
            status: 200,
            message: "berhasil",
        });

    });
});

/**
 * @swagger
 * /api/contact/update:
 *   post:
 *     summary: post the Contact
 *     tags: [Contact]
 *     consumes: 
 *      - multipart/form-data
 *     parameters:
 *       - in: formData
 *         name: id
 *         schema:
 *           type: string
 *         required: false
 *         description: The contact id
 *       - in: formData
 *         name: img
 *         schema:
 *           type: file
 *         required: false
 *         description: The contact img
 *     security:
 *      -  basicAuth: []
 *     responses:
 *       200:
 *         description: The contact description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/contact'
 *       404:
 *         description: The contact was not found
 */
router.post("/update", token, async (req, res) => {

    const id = req.body.id;
    //lets validate data
    const { error } = contactupdateValidator(req.body);
    if (error)
        return res.status(400).send({
            status: 400,
            error: error.details[0].message,
        });

    //check contack
    await contact.findById({ _id: id }).exec(function (err, data) {
        // data kosong
        if (err || !contact)
            return res.status(400).send({
                satatus: 400,
                error: "id yang di input salah",
            });
    });

    // console.log(test);
    try {
        const coba = await contact.findByIdAndUpdate(
            {
                _id: id,
            },
            {
                $set: {
                    name: req.body.name,
                    alamat: req.body.alamat,
                    address: req.body.address,
                },
            }
        );
        res.status(200).send({
            status: 200,
            message: "berhasil update",
            data: coba
        });
    } catch (err) {
        res.status(400).send(err);
    }
});



// upload foto
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "Public/storage/");
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    },
});


const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5,
    },
    fileFilter: fileFilter,
});
//Validation

router.post("/uploadFoto", upload.single("img"), async (req, res) => {

    console.log(req.body.id);

    const cekcontack = await contact.findOne({ _id: req.body.id });
    if (!cekcontack)
        return res.status(400).send({
            status: 400,
            error: "id contact belum terdaftar",
        });

    try {
        const update = await contact.findByIdAndUpdate(
            {
                _id: req.body.id,
            },
            {
                $set: {
                    picture: req.file.path,
                },
            }
        );

        res.status(200).send({
            status: 200,
            message: "berhasil",
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}); //end route upload


module.exports = router;
