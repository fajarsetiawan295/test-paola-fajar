const router = require("express").Router();
const User = require("../Model/users");
const { RegisterValidator, LoginValidator } = require("../Validator/users");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { encode } = require("../../Config/Security/TokenJwt");
const token = require("../../Config/Security/VerifyToken");

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         name:
 *           type: string
 *           description: The user name
 *         email:
 *           type: string
 *           description: The User email
 *         password:
 *           type: string
 *           description: The User password
 *       example:
 *         name: fajar setiawan
 *         email: fajarsetiawan295@gmail.com
 *         password: "fajar123456"
 */

/**
 * @swagger
 * tags:
 *   name: User
 *   description: The User managing API
 */

/**
 * @swagger
 * /api/users/Register:
 *   post:
 *     summary: Create a new user
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The User was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       500:
 *         description: Some server error
 */

router.post("/Register", async (req, res) => {
  const { error } = RegisterValidator(req.body);
  if (error)
    return res.status(400).send({
      status: 400,
      error: error.details[0].message,
    });

  //check user
  const checkUser = await User.findOne({ email: req.body.email });
  if (checkUser)
    return res.status(400).send({
      status: 400,
      error: "Email Already exists",
    });
  // hash password

  const Salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(req.body.password, Salt);
  //create
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
  });
  try {
    const savedUser = await user.save();
    res.status(200).send({
      status: 200,
      data: "berhail create",
    });
  } catch (err) {
    res.status(400).send({ status: 400, error: "Eror" });
  }
}); //end route register



/**
 * @swagger
 * components:
 *   schemas:
 *     Login:
 *       type: object
 *       required:
 *         - email
 *         - password
 *       properties:
 *         email:
 *           type: string
 *           description: The User email
 *         password:
 *           type: string
 *           description: The User password
 *       example:
 *         email: fajarsetiawan295@gmail.com
 *         password: "fajar123456"
 */

/**
 * @swagger
 * /api/users/Login:
 *   post:
 *     summary: Create a new user
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Login'
 *     responses:
 *       200:
 *         description: The User was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Login'
 *       500:
 *         description: Some server error
 */
router.post("/login", async (req, res) => {
  // validator login

  const { error } = LoginValidator(req.body);
  if (error)
    return res.status(400).send({
      status: 400,
      error: error.details[0].message,
    });

  //check user
  const checkUser = await User.findOne({ email: req.body.email });
  if (!checkUser)
    return res.status(400).send({
      status: 400,
      error: "Email not registered",
    });

  //check password
  const checkPassword = await bcrypt.compare(
    req.body.password,
    checkUser.password
  );
  if (!checkPassword)
    return res.status(400).send({
      status: 400,
      error: "Invalid password",
    });

  //create Token

  const token = jwt.sign({ _id: checkUser._id }, process.env.TOKEN_SECRET);
  res.header("auth-token", token).send({
    status: 200,
    token: token,
  });
}); //end route login


module.exports = router;
