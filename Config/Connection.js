const express = require('express');
const dotenv = require('dotenv');


dotenv.config();
switch (process.env.NODE_ENV) {
    case 'production':
        var database = process.env.DB_CONNECT_production;
        var port = process.env.PORT__CONNECT_production;
        break;
    case 'testing':
        var database = process.env.DB_CONNECT_test;
        var port = process.env.PORT__CONNECT_test;
        break;
    default:
        var database = process.env.DB_CONNECT_dev;
        var port = process.env.PORT__CONNECT_dev;

}

const configurasi = {
    conn: database,
    port: port
}

module.exports = configurasi;