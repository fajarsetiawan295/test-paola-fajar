const jwt = require('jsonwebtoken');


exports.encode = function (req, res, next) {

    const token = req.header('auth-token');
    if (!token) return {
        'status': 401,
        'message': 'Access Denied',
    }
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = verified;
        return req.user;
    } catch (err) {
        return {
            'status': 400,
            'message': 'Invalid Token',
        }
    }
}