const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const http = require("http");
const path = require("path");
const fs = require("fs");
const httpServer = http.createServer(app);
const connectiondatabase = require("./Config/Connection");
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

dotenv.config();
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

// var routes = require("./Route/Api");
// routes(app);
const options = {

  definition: {
    openapi: "3.0.0",
    info: {
      title: "Library API",
      version: "1.0.0",
      description: "A simple Express Library API"
    },
    servers: [
      {
        url: "http://localhost:3001/"
      }
    ],
  },
  apis: ["./Api/Controller/*.js"],
  components: {
    securitySchemes: {
      ApiKeyAuth: {
        type: "apiKey",
        in: "header",
        name: "X-API-Key"
      }
    }
  },
  security: [
    { apiKey: [] }
  ]
};

mongoose
  .connect(connectiondatabase.conn, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log(`connection to database established`);
  })
  .catch((err) => {
    console.log(`db error ${err.message}`);
    process.exit(-1);
  });
const specs = swaggerJsDoc(options);
app.use("/api", require("./Route/Api"));
app.use("/api-swagger", swaggerUI.serve, swaggerUI.setup(specs));

app.use(express.static(path.join(__dirname, "./")));

var port = connectiondatabase.port;
app.listen(port, function () {
  console.log("Listen " + port);
});
